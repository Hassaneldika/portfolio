const mongoose = require('mongoose');

const aboutSchema = new mongoose.Schema({
    company_name: String,
    company_description: String,
    company_image: String,
    company_views: [
        {
            title: String,
            description: String
        },
        {
            title: String,
            description: String
        },
         {
            title: String,
            description: String
        }
    ]
});

const AboutUs = mongoose.model('AboutUs', aboutSchema);

module.exports = AboutUs;