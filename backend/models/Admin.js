const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    username: String,
    password: String,
    token: String
});

const Admin = mongoose.model('Admin', adminSchema);

module.exports = Admin;