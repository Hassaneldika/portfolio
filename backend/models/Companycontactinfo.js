const mongoose = require ("mongoose");

const CompanycontactinfoSchema = new mongoose.Schema (
    {
        company_slogant: String,
        company_address: String, 
        company_phone: String,
        company_email: String,
        company_logo: String
    }
);
const Companycontactinfo = mongoose.model("Companycontactinfo", CompanycontactinfoSchema);

module.exports = Companycontactinfo;
