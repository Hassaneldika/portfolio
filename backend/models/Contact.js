const mongoose = require('mongoose');

const contactSchema = new mongoose.Schema({
    name: String,
    email: String,
    subject: String,
    message: String,
    time: String
});

const ContactUs = mongoose.model('ContactUs', contactSchema);

module.exports = ContactUs;