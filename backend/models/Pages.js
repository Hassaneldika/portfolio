const mongoose = require('mongoose');

const pagesSchma = new mongoose.Schema({
    home_image: String,
    contact_image: String,
    home_title: String,
    home_description: String
});

const Pages = mongoose.model("Pages", pagesSchma);

module.exports = Pages;
