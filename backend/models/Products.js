const mongoose = require('mongoose');
const Categories = require ('./Categories');

const productsSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String,
    price: Number,
    discount: Number,
    category: {type: mongoose.SchemaTypes.ObjectID, ref: "Categories"}
});

const Products = mongoose.model('Products', productsSchema);

module.exports = Products;