const mongoose = require('mongoose');

const visionSchema = new mongoose.Schema({
    title: String,
    description: String
})

const Vision = mongoose.model('Visions', visionSchema);

module.exports = Vision;