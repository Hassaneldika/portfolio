const router = require('express').Router();
const AboutUs = require('../models/About');

router.get('/', (req, res) => {
    AboutUs.find((err, val) => {
        if(val) {
          res.json({status: 200, message: val});
        }
    })
});

module.exports = router;