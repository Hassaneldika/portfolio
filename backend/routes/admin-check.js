const router = require('express').Router();
const Admin = require('../models/Admin');
const jwt = require('jsonwebtoken');

router.post('/', async (req, res) => {
    const user = req.body.username;
    const pass = req.body.password;
    const User = await Admin.findOne({username: user, password: pass});
    if (User) {
        const token = jwt.sign(
            {
                username: User.username
            },
            'secret123', {expiresIn: '12h'}
        )

        Admin.findOneAndUpdate({username: user, password: pass}, {$set: {token: token}}, {new: true}, (err, val) => {
            if(err) {
                res.json('Error');
            }
        })

        return res.json({status: 200, user: token})
    } else {
        return res.json({status: 404, message: "User not found"})
    }
})

router.get('/all', (req, res) => {
    Admin.find((err, val) => {
        if(val){
            res.json({status: 200, message: val});
        }
    })
})

router.patch('/update', (req, res) => {
    Admin.findOneAndUpdate({_id: "62c42cc50f93c71cbbe431a1"}, {$set: {username: req.body.username, password: req.body.password}}, {new: true}, (err, val) => {
        if(err){
            res.status(500).send({status: 500, message: "error"});
        } else {
            Admin.find((err, val) => {
                res.json({status: 200, message: val});
            })
        }
    })
})

module.exports = router;