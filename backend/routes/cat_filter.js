const router = require('express').Router();
const Products = require('../models/Products');

router.post("/",(req,res)=>{
    const ID = req.body.ID
    const names = req.body.Name
    Products.find({category:ID}).populate("category",["name"],{match:{name:names}}).then(products =>{ res.json(products)
    })})

module.exports = router;