const router = require('express').Router();
const Categories = require('../models/Categories');

router.get('/', (req, res) => {
    Categories.find((err, val) => {
        if(val) {
          res.json({status: 200, message: val});
        }
    })
});

module.exports = router;