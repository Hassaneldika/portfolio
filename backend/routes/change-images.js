const router = require('express').Router();
const Pages = require('../models/Pages');
const Info = require('../models/Companycontactinfo');

router.patch('/', (req, res) => {
    Pages.findOneAndUpdate({_id: '62bd7c79339125aa4801b775'}, {$set: {home_image: req.body.heroImg, contact_image: req.body.contactImg, home_title: req.body.homeTitle, home_description: req.body.homeDesc}}, {new: true}, (err, val) => {
        if(val) {
            Pages.find((err, val) => {
                res.json({status: 200, message: val});
            })
        }
    });
})

router.patch('/logo', (req, res) => { 
    Info.findOneAndUpdate({_id: "62bc3da9b565a08594f8b9a9"}, {$set: {company_logo: req.body.logo}}, {new: true}, (err, val) => {
        if(val) {
            Pages.find((err, val) => {
                res.json({status: 200, message: val});
            })
        }
    });
})

module.exports = router;