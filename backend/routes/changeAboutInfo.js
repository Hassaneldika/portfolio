const router = require('express').Router();
const About = require('../models/About');
const multer = require('multer');

const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, '../frontend/public/assets')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});

const upload = multer({storage: fileStorageEngine});

router.patch('/', (req, res) => {
    About.findOneAndUpdate({_id: "62c692076e6d5110aa50ca4f"}, {$set: {company_name: req.body.c_name, company_description: req.body.c_description, company_image: req.body.c_image}}, {new: true}, (err, val) => {
        if(err){
            res.status(500).send({status: 500, message: "error"});
        } else {
            About.find((err, val) => {
                res.json({status: 200, message: val});
            })
        }
    })
})

router.post('/image', upload.single("image"), (req, res) => {
    res.json({status: 200});
})

module.exports = router;