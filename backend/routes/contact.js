const router = require('express').Router();
const { default: mongoose } = require('mongoose');
const ContactUs = require('../models/Contact');

router.post('/', async (req, res) => {
    const newMsg = new ContactUs({name: req.body.name, email: req.body.email, subject: req.body.subject, message: req.body.message, time: req.body.time});
    await newMsg.save();
});

module.exports = router;