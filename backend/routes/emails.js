const router = require('express').Router();
const Contact = require('../models/Contact');

router.get('/', (req, res) => {
    Contact.find((err, val) => {
        if(val){
            res.json({status: 200, message: val});
        } else {
            res.json('not found');
        }
    })
});

module.exports = router;
