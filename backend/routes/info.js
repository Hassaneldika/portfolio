const router = require('express').Router();
const Companycontactinfo = require('../models/Companycontactinfo');

router.get('/', (req, res) => {
    Companycontactinfo.find((err, val) => {
        if(val){
            res.json({status: 200, message: val});
        }
    })
});

module.exports = router;
