const router = require('express').Router();
const Pages = require('../models/Pages');

router.get("/", (req, res) => {
    Pages.find((err, val) => {
        if(val){
            res.json({status: 200, message: val});
        }
    })
});

module.exports = router;