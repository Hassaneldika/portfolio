const router = require('express').Router();
const Products = require('../models/Products');

router.get('/', (req, res) => {
    Products.find().populate("category",["name"]).then(products =>{ res.json({status: 200, message: products})
    })})

module.exports = router;