const router = require('express').Router();
const Products = require('../models/Products');

router.get('/:id', (req, res) => {
    Products.find(({_id: req.params.id}), (err, val) => {
        if(val){
            res.json({status: 200, message: val});
        } else {
            res.status(404).json({status: 404, message: "Product not Found"});
        }
    })
});

module.exports = router;