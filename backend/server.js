const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const getProducts = require('./routes/product');
const getContact = require('./routes/contact');
const getPages = require('./routes/pages');
const getCategory = require('./routes/categories');
const getAbout = require('./routes/about');
const getInfo = require('./routes/info');
const getOneProduct = require('./routes/single-product');
const getFilter = require('./routes/cat_filter');
const adminCheck = require('./routes/admin-check');
const changeAbout = require('./routes/changeAboutInfo');
const changeContact = require('./routes/ChangeContactInfo');
const emails = require('./routes/emails');
const changeImages = require('./routes/change-images');
const Products = require('./models/Products');
const Admin = require('./models/Admin');
const About = require('./models/About')
// const Info = require('./models/Companycontactinfo');
// const ContactUs = require('./models/Contact');
// const Pages = require('./models/Pages')
// const Categories = require('./models/Categories');

require('dotenv').config();

const app = express();
const port = 3001;

app.use(cors());
app.use(express.json());

const uri ="mongodb+srv://root:root123@cluster0.jc5vd.mongodb.net/?retryWrites=true&w=majority";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection successfully");
})


app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});

app.use('/', getCategory);

app.use('/about', getAbout);

app.use('/pages', getPages);

app.use('/add', getContact);

app.use('/products', getProducts);

app.use('/info', getInfo);

app.use('/product/', getOneProduct);

app.use('/readit', getFilter);

app.use('/admin', adminCheck);

app.use('/change-about', changeAbout);

app.use('/change-contact', changeContact);

app.use('/emails', Emails);

app.use('/change-images', changeImages);

// app.get('/test', (req, res) => {
//   Info.find((err, val) => {
//     res.json(val);
//   })
// })

// app.get('/contact', (req, res) => {
//   ContactUs.find((err, val) => {
//     res.json(val);
//   })
// })

// app.post('/create', async (req, res) => 
// {  
//     const x = new Pages({ home_image: req.query.hi, contact_image: req.query.ci, home_title: req.query.ht,home_description: req.query.hd});
//     await x.save()
//     res.send({message:200});
// })

// app.get("/", (req, res)=>{
//     Categories.find((err, val)=>{
//         res.json(val);
//     })
// })

// app.post('/create', async (req, res) => {
//   let ca = new Categories({name: req.query.name});
//   await ca.save();
// })

// app.post('/create', async (req, res) => 
// {  
//     const x = new Products({ name: req.query.na, image: req.query.im, description: req.query.de,price: req.query.pr, discount: req.query.di, category: req.query.ca});
//     await x.save()
//     res.send({message:200});
// })

// app.get("/pro", (req, res) => {
//   Products.find((err, val) => {
//     res.json(val);
//   })
// })

// app.patch('/update', (req, res) => {
//   Products.findOneAndUpdate({name: req.query.name},{ $set: { image: req.query.image, description: req.query.des} }, { new: true },(err, val)=>{
//     if(val){
//       res.send({status:200});
//     }
//   } )
// })

// app.post('/admin', async (req, res)=>{
//   const ad = new Admin({username: req.query.name, password: req.query.password});
//   await ad.save();
//   res.send({status: 200});
// })

// app.get('/getad', (req, res)=>{
//   Admin.find((err, val)=>{
//     res.send(val);
//   })
// })

// app.post('/create', async (req, res) => {
//   let n = new About({company_name: req.query.na, company_description: req.query.de, company_image: req.query.im, company_views: [{ title: req.query.ti1, description: req.query.de1}, { title: req.query.ti2, description: req.query.de2}, { title: req.query.ti3, description: req.query.de3}]});
//   await n.save();
//   res.send({status: 200}); 
// })