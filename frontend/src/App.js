import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from "./components/Home/Home";
import About from "./components/About/About";
import Contact from "./components/Contact/Contact";
import Login from "./components/Login/login";
import Product from "./components/Product/product";
import ChangeLoginInfo from "./components/Admin/ChangeLoginInfo/change-login-info";
import ChangeAboutInfo from "./components/Admin/ChangeAboutInfo/change_about_info";
import ChangeContactInfo from "./components/Admin/ChangeContactInfo/change_contact_info";
import Emails from "./components/Admin/Emails/emails";
import ChangeImages from "./components/Admin/ChangeImages/change-images";

function App() {
  return (
    <Router>
      <>
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/login" element={<Login />} />
          <Route path="/product/:id" element={<Product />} />
          <Route path="/login-info" element={<ChangeLoginInfo />} />
          <Route path="/about-info" element={<ChangeAboutInfo />} />
          <Route path="/contact-info" element={<ChangeContactInfo />} />
          <Route path="/emails" element={<Emails />} />
          <Route path="/change-images" element={<ChangeImages />} />
        </Routes>
      </>
    </Router>
  );
}

export default App;
