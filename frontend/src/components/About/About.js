import React, {useState, useEffect} from "react";
import "./About.css";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";

function About() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    FetchData11();
  }, [])

  const FetchData11 = async () => {
    await fetch("http://localhost:3001/about")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  if (loading) return "Loading data...";
  if (error) return "Error...";

  return (
    <>
    <Navbar />
    <div className="about">
      <div className="aboutBottom">
        <h2> ABOUT US</h2>
        <h3 className="company_name">{data.message[0].company_name}</h3>
        <p>
        {data.message[0].company_description}
        </p>
        <img src={data.message[0].company_image} alt="Logo"></img>
        <h5>Our Vision</h5>

        <section className="container">
          {data.message[0].company_views.map((info)=>
           <div className="card">
           <div className="info">
             <h3>{info.title}</h3>
             <span>
             {info.description}
             </span>
           </div>
         </div>
          )}
         

        </section>
      </div>
    </div>
    <Footer />
    </>
  );
}

export default About;