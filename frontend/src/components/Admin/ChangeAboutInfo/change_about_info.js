import React, { useEffect, useState } from "react";
import "./change_about_info.css";
import Sidebar from "../Sidebar/Sidebar";

export default function ChangeAboutInfo() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loading1, setLoading1] = useState(false);
  const [newName, setNewName] = useState(null);
  const [newDescription, setNewDescription] = useState(null);
  const [image, setImage] = useState(null);
  
  setTimeout(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      window.location.href = "/login";
    }
  }, 10);

  useEffect(() => {
    FetchData18();
  }, []);

  const FetchData18 = async () => {
    await fetch("http://localhost:3001/about")
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching Data, " + error);
        setError(error);
      });
  };

  const handleSubmit = () => {
    let msg = {};

    if (newName) {
      msg["c_name"] = newName;
    }
    if (newDescription) {
      msg["c_description"] = newDescription;
    }
    if (image) {
      msg['c_image'] = `/assets/${image.name}`;
    }

    setLoading(true);
    fetch("http://localhost:3001/change-about", {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(msg)
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
        setNewName(null);
        setNewDescription(null);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching Data, " + error);
        setError(error);
      });
  };

  const handleSubmitImage = () => {
    if (image) {
      const formData = new FormData();
      formData.append("image", image);

      setLoading1(true);
      fetch("http://localhost:3001/change-about/image", {
      method: "POST",
      body: formData
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then(() => {
        setImage(null);
        setLoading1(false);
      })
      .catch((error) => {
        console.error("Error fetching Data, " + error);
        setError(error);
      });
    }
  }

  const dualFunction = (e) => {
    e.preventDefault();
    handleSubmit();
    handleSubmitImage();
  }

  if (loading || loading1) return "Loading data...";
  if (error) return "Error...";

  return (
    <div className="change_about_wrapper">
      <Sidebar />
      <div className="change_about">
        <form className="aboutBottom" onSubmit={dualFunction}>
          <h2> ABOUT US</h2>
          <input
            type={"text"}
            className="company_name"
            placeholder={data.message[0].company_name}
            value={newName}
            onChange={(e) => {
              setNewName(e.target.value);
            }}
          />
          <textarea
            type={"text"}
            className="company_descr"
            placeholder={data.message[0].company_description}
            value={newDescription}
            onChange={(e) => {
              setNewDescription(e.target.value);
            }}
          />
          
          <img src={data.message[0].company_image} alt="Logo" />
          <input className="btn_edit_about_img" type={'file'} onChange={(e)=>{setImage(e.target.files[0])}} />
          <h5>Our Vision</h5>

          <section className="container">
            {data.message[0].company_views.map((info) => (
              <div className="card">
                <div className="info">
                  <input
                    type={"text"}
                    className="vision_title"
                    placeholder={info.title}
                  />
                  <textarea
                    type={"text"}
                    className="vision_desc"
                    placeholder={info.description}
                  />
                </div>
                <div className="delete_vision_checkboxs">
                  <i class="fa-solid fa-trash"></i>
                  <input type={"checkbox"} className="delete_vision_checkbox" />
                </div>
              </div>
            ))}
          </section>
          <div className="change_about_btns">
            <button className="can">Cancel</button>

            <button type="submit" className="sub">
              Apply
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
