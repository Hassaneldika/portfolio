import React, { useEffect, useState } from 'react';
import './change_contact_info.css'
import Sidebar from '../Sidebar/Sidebar';

export default function ChangeContactInfo() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [address, setAddress] = useState(null);
  const [email, setEmail] = useState(null);
  const [phone, setPhone] = useState(null);
  const [slogan, setSlogan] = useState(null);
  const [loading, setLoading] = useState(true);

  setTimeout(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      window.location.href = "/login";
    }
  }, 10);

  useEffect(() => {
    Fetch20();
  }, [])

  const Fetch20 = async () => {
    await fetch("http://localhost:3001/info")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  const Submit = (e) => {
    e.preventDefault();
    let msg = {};

    if(address){msg['address'] = address};
    if(phone){msg['phone'] = phone};
    if(email){msg['email'] = email};
    if(slogan){msg['slogan'] = slogan};

    setLoading(true);
        fetch("http://localhost:3001/change-contact", {
            method: 'PATCH',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(msg)
        })
          .then(response => {
            if (response.ok) {
              return response.json();
            } throw response;
          })
          .then(data => {
            setData(data);
            setPhone(null);
            setAddress(null);
            setEmail(null);
            setSlogan(null);
            setLoading(false);
          })
          .catch(error => {
            console.error('Error fetching Data, ' + error);
            setError(error);
          })
  }

  if (loading) return "Loading data...";
  if (error) return "Error...";

    return(
        <>
        <Sidebar />
        <div className='change_contact_wrapper'>
        <h1>Change Contact Information</h1>
        <form className='change_contact_info' onSubmit={Submit}>
            <lablel className='change_contact_label' for='Address'>Address</lablel>
            <input type={'text'} id='Address' className='change_contact_input' placeholder={data.message[0].company_address} value={address} onChange={(e)=>{setAddress(e.target.value)}}/>

            <lablel className='change_contact_label' for='Email'>Email</lablel>
            <input type={'email'} id='Email' className='change_contact_input' placeholder={data.message[0].company_email} value={email} onChange={(e)=>{setEmail(e.target.value)}}/>

            <lablel className='change_contact_label' for='Phone'>Phone</lablel>
            <input type={'text'} id='Phone' className='change_contact_input' placeholder={data.message[0].company_phone} value={phone} onChange={(e)=>{setPhone(e.target.value)}}/>

            <lablel className='change_contact_label' for='Slogan'>Slogan</lablel>
            <input type={'text'} id='Slogan' className='change_contact_input' placeholder={data.message[0].company_slogant} value={slogan} onChange={(e)=>{setSlogan(e.target.value)}}/>

            <div className='change_contact_buttons'>
                <button type={'reset'} className='change_contact_button'>Cancel</button>
                <button type={'submit'} className='change_contact_button'>Apply</button>
            </div>
        </form>
        </div>
        </>
    )
}