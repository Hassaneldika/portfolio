import React, { useEffect, useState } from 'react';
import './change-images.css';
import Sidebar from '../Sidebar/Sidebar';

export default function ChangeImages() {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loading1, setLoading1] = useState(false);
    const [loading2, setLoading2] = useState(false);
    const [loading3, setLoading3] = useState(false);
    const [loading4, setLoading4] = useState(false);
    const [logo, setLogo] = useState(null);
    const [contactImg, setContactImg] = useState(null);
    const [heroImg, setHeroImg] = useState(null);
    const [homeTitle, setHomeTitle] = useState(null);
    const [homeDesc, setHomeDesc] = useState(null);

    const FetchData1 = async () => {
        await fetch("http://localhost:3001/pages")
          .then(response => {
            if (response.ok) {
              return response.json();
            } throw response;
          })
          .then(data => {
            setData(data);
            setLoading(false);
          })
          .catch(error => {
            console.error('Error fetching Data, ' + error);
            setError(error);
          })
      }

      useEffect(()=>{
        FetchData1();
      }, [])

      const handleSubmit = () => {
        let msg = {};
        let msg1 = {};

        if (homeTitle) {
          msg["homeTitle"] = homeTitle;
        }
        if (homeDesc) {
          msg["homeDesc"] = homeDesc;
        }
        if (heroImg) {
          msg["heroImg"] = `/assets/${heroImg.name}`;
        }
        if (contactImg) {
          msg["contactImg"] = `/assets/${contactImg.name}`;
        }
        if (logo) {
          msg1["logo"] = `/assets/${logo.name}`;
        }

        setLoading(true);
    fetch("http://localhost:3001/change-images", {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(msg)
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
        setHomeDesc(null);
        setHomeTitle(null);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching Data, " + error);
        setError(error);
      });

      setLoading4(true);
      fetch("http://localhost:3001/change-images/logo", {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(msg1)
      })
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
          throw response;
        })
        .then((data) => {
          setData(data);
          setLogo(null);
          setLoading4(false);
        })
        .catch((error) => {
          console.error("Error fetching Data, " + error);
          setError(error);
        });
  
      }

      const handleSubmitLogo = () => {
        if (logo) {
          const formData = new FormData();
          formData.append("image", logo);

          setLoading1(true);
          fetch("http://localhost:3001/change-about/image", {
          method: "POST",
          body: formData
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw response;
          })
          .then(() => {
            setLogo(null);
            setLoading1(false);
          })
          .catch((error) => {
            console.error("Error fetching Data, " + error);
            setError(error);
          });
        }    
      }

      const handleSubmitHerImg = () => {
        if (heroImg) {
          const formData = new FormData();
          formData.append("image", heroImg);

          setLoading2(true);
          fetch("http://localhost:3001/change-about/image", {
          method: "POST",
          body: formData
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw response;
          })
          .then(() => {
            setHeroImg(null);
            setLoading2(false);
          })
          .catch((error) => {
            console.error("Error fetching Data, " + error);
            setError(error);
          });
        }    
      }

      const handleSubmitContactImg = () => {
        if (contactImg) {
          const formData = new FormData();
          formData.append("image", contactImg);

          setLoading3(true);
          fetch("http://localhost:3001/change-about/image", {
          method: "POST",
          body: formData
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            throw response;
          })
          .then(() => {
            setContactImg(null);
            setLoading3(false);
          })
          .catch((error) => {
            console.error("Error fetching Data, " + error);
            setError(error);
          });
        }    
      }

      const multipleSubmit = (e) => {
        e.preventDefault();
        handleSubmit();
        handleSubmitContactImg();
        handleSubmitHerImg();
        handleSubmitLogo();
      }

      if (error) return "Error...";
      if (loading || loading1 || loading2 || loading3 || loading4) return "Loading data...";

    return (
        <>
            <Sidebar />
            <form className='change_images_wrapper' onSubmit={multipleSubmit}>
                <h2 className='change_images_title'>Other Changes</h2>
                <div className='change_images_body'>
                    <div className='change_home'>
                        <h3>Home</h3>
                        <lablel for='home_title'>Title</lablel>
                        <input type={'text'} id='home_title' placeholder={data.message[0].home_title} value={homeTitle} onChange={(e)=>{setHomeTitle(e.target.value)}}/>

                        <lablel for='home_desc'>Description</lablel>
                        <input type={'text'} id='home_desc' placeholder={data.message[0].home_description} value={homeDesc} onChange={(e)=>{setHomeDesc(e.target.value)}}/>
              
                        <lablel for='home_imgg'>Hero Image</lablel>
                        <input type={'file'} id='home_imgg' value={heroImg} onChange={(e)=>{setHeroImg(e.target.value)}}/>
                    </div>
                    <div className='change_other_images'>
                        <h3 className='contact_title'>Contact-Us Image</h3>
                        <input type={'file'} id='contact_imgg' value={contactImg} onChange={(e)=>{setContactImg(e.target.value)}}/>

                        <h3 className='logo_title'>Logo</h3>
                        <input type={'file'} id='logo_imgg' value={logo} onChange={(e)=>{setLogo(e.target.value)}}/>
                    </div>
                </div>
                <div className='change_images_btns'>
                  <button type={'submit'} className='change_images_submit'>Apply</button>
                  <button type={'reset'} className='change_images_cancel'>Cancel</button>
                </div>
            </form>
        </>
    )
}