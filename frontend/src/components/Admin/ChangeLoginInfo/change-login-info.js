import React, { useEffect, useState } from 'react';
import './Change_login_info.css'
import Footer from '../../Footer/Footer';
import Sidebar from '../Sidebar/Sidebar';

export default function ChangeLoginInfo() {
    setTimeout(() => {
        const token = localStorage.getItem('token');
        if (!token ) {
            window.location.href = '/login';
        }
    }, 10)

    const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirm, setConfirm] = useState(null);
  const [errorConfirm, setErrorConfirm] = useState("change_text")

  useEffect(() => {
    FetchData();
  }, [])

  const FetchData = async () => {
    await fetch("http://localhost:3001/admin/all")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  const handleSubmit2 = (e) => {
    e.preventDefault();

    if(password !== confirm) {
      setErrorConfirm('error_confirm')
    } else {
      const msg = {username, password};
      setLoading(true);

      fetch("http://localhost:3001/admin/update", {
      method: 'PATCH',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(msg)
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setUsername(null);
      setPassword(null);
      setConfirm(null);
      setErrorConfirm('change_text');
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })

    }
  }

  if (loading) return "Loading data...";
  if (error) return "Error...";

    return (
        <>
            <Sidebar />
            <div className='change_login_info_container'>
                <h1>Change Login Information</h1>
                <form className='change_login_info' onSubmit={handleSubmit2}>
                    
                        <label className='change_title' for="change_username">UserName</label>
                        <input type="text" className='change_text' placeholder={data.message[0].username} id='change_username' value={username} onChange={(e)=>{setUsername(e.target.value)}} required></input>
                        
                    
                    
                        <label className='change_title' for="change_password">Password</label>
                        <input type="password" className={errorConfirm} placeholder={data.message[0].password} id="change_password" value={password} onChange={(e)=>{setPassword(e.target.value)}} required></input>
                       
                    
                    
                        <label className='change_title' for="change_confirm_password">Confirm Passwrord</label>
                        <input type="password" className={errorConfirm} placeholder={data.message[0].password} id='change_confirm_password' value={confirm} onChange={(e)=>{setConfirm(e.target.value)}} required></input>
                       
                        <div className='change_btns'>
                    
                        <button className='can'>Cancel</button>
                    
                    
                        <button type='submit' className='sub'>Apply</button>

                        </div>
                    
                    </form>
            </div>
            <Footer />
        </>
    )
}