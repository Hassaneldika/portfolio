import React, { useEffect, useState } from "react";
import Sidebar from "../Sidebar/Sidebar";
import "./emails.css";
import Message from "../../message/message";

function Emails() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    FetchData19();
  }, []);

  const FetchData19 = async () => {
    await fetch("http://localhost:3001/emails")
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching Data, " + error);
        setError(error);
      });
  };

  if (loading) return "Loading data...";
  if (error) return "Error...";

  return (
    <div className="emails_wrapper">
      <Sidebar />
      <div className="emails">
        <div className="emails_info">
        {data.message.map((info) => (
          <Message name={info.name} message={info.message} email={info.email} time={info.time}/>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Emails;
