import React, {useState, useEffect} from 'react';
import './Sidebar.css';
import {Link} from 'react-router-dom';

function Sidebar() {
    const [data, setData] = useState(null);
    const [data1, setData1] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loading1, setLoading1] = useState(true);

  useEffect(() => {
    FetchData21();
    FetchData22();
}, [])

const FetchData21 = async () => {
  await fetch("http://localhost:3001/info")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
}
const FetchData22 = async () => {
    await fetch("http://localhost:3001/about")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data1 => {
      
      setData1(data1);
      setLoading1(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

if (error) return "Error...";
if (loading || loading1) return "Loading data...";

  return (
    <>
    <section className='side_bar'>
        <div className='top'>
            <div className='brand'>
            <img src={data.message[0].company_logo} alt='logo'/><span>{data1.message[0].company_name}</span>
            </div>
            <div className='toggle'>

            </div>

            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-clipboard-check"></i><Link to={'/login-info'}><span>Change Login Info</span></Link></li>
                </ul>
            </div>
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-address-card"></i><Link to={'/about-info'}><span>Change About Us Info</span></Link></li>
                </ul>
            </div>
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-address-book"></i><Link to={'/contact-info'}><span>Change Contact Info</span></Link></li>

                </ul>
            </div>
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-envelope-circle-check"></i><Link to={'/emails'}><span>Emails</span></Link></li>
                </ul>
            </div>
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-image"></i><Link to={'/change-images'}><span>Other Edits</span></Link></li>
                </ul>
            </div>
           
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-p"></i><a href='#'><span>Products</span></a></li>
                </ul>
            </div>
            <div className='links'>
                <ul>
                    <li><i class="fa-solid fa-arrow-right-from-bracket"></i><a href='#'><span>Logout</span></a></li>
                </ul>
            </div>
        </div>
    </section>
      
    </>
  )
}



export default Sidebar


