import React from 'react';
import { Link } from 'react-router-dom';
import './category.css';

export default function Category({ name, img, category, price, discount, id }) {
  return (
    <Link to={`/product/${id}`} className='product_wrapper category'>
      <img src={img} alt='Product' />
      <h2 className='product_name'>{name}</h2>
      <h6 className='product_category'>{category}</h6>
      <div className='product_value'>
        <div className='price'>
          <h3 className='product_price'><s>{discount !== 0 ? '$' + price : null}</s></h3>
          <h3 className='product_price_discounted'>{discount !== 0 ? '$' + (price * (100 - discount) / 100) : `$${price}`}</h3>
        </div>
        <h4 className='product_discount'>{discount !== 0 ? discount + '%' : null}</h4>
      </div>
    </Link>

  )
}