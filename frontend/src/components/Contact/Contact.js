import React, {useEffect, useState} from 'react';
import './Contact.css'
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';


export default function Contact() {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(true);
  const [conf, setConf] = useState('confirmation');
  const time = new Date();

  useEffect(() => {
    FetchData10();
  }, [])

  const FetchData10 = async () => {
    await fetch("http://localhost:3001/pages")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  const Submit = (e) => {
    e.preventDefault();
    const msg = {name, email, subject, message, time};


    fetch("http://localhost:3001/add", {
      method: 'POST',
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify(msg)
    })
    setConf('activee');
    setName('');
    setEmail('');
    setSubject('');
    setMessage('');
    setTimeout(()=>{
      setConf('confirmation');
    }, 5000);

  }
  
  if (loading) return "Loading data...";
  if (error) return "Error...";

  return (
    <>
    <Navbar />
    <div className="contact">
      <div className="section1">
        <h1>Contact</h1>
        <form onSubmit={Submit}>
        <div className="name">
          <label>Name</label>
          <input type="text" className="text-box" placeholder="Name" required value={name} onChange={(e) => setName(e.target.value)}></input>
        </div>
        <div className="email">
          <label>Email</label>
          <input type="email" className="text-box" placeholder="Email" required value={email} onChange={(e) => setEmail(e.target.value)}></input>
        </div>
        <div className="subject">
          <label>Subject</label>
          <input type="text" className="text-box" placeholder='Subject' required value={subject} onChange={(e) => setSubject(e.target.value)}></input>
        </div>
        <div className="message">
          <label>Message</label>
          <textarea placeholder="Write something.." required value={message} onChange={(e) => setMessage(e.target.value)}></textarea>
        </div>
        <div className="send">
          <input type="submit" className='button' value={'SEND'}></input>
          <div className={conf}>Message sent successfully!</div>
        </div>
        </form>
      </div>
      <div className="section2">
        <img src={data.message[0].contact_image} alt="Contact-us_image"></img>
        
      </div>
    </div>
    <div className={conf}>Message sent successfully!</div>
    <Footer />
    </>
  )
}