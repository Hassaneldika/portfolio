import React, {useEffect, useState} from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';

function Footer() {
  const [data, setData] = useState(null);
  const [data1, setData1] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);

  useEffect(() => {
    Fetch();
    Fetch1();
    
  }, [])

  const Fetch = async () => {
    await fetch("http://localhost:3001/info")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  const Fetch1 = async () => {
    await fetch("http://localhost:3001/about")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data1 => {
      
      setData1(data1);
      setLoading2(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  if (loading || loading2) return "Loading data...";
  if (error) return "Error...";
  // console.log(data);

  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
          {data.message[0].company_slogant}
        </p>
      </section>
      <div class='footer-links'>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Address</h2>
            <p>{data.message[0].company_address}</p>
          </div>
          <div class='footer-link-items'>
            <h2>Phone</h2>
            <p>{data.message[0].company_phone}</p>
          </div>
        </div>
        <div className='footer-link-wrapper'>
          <div class='footer-link-items'>
            <h2>Email</h2>
            <p>{data.message[0].company_email}</p>
          </div>
        </div>
      </div>
      <section class='social-media'>
        <div class='social-media-wrap'>
          <div class='footer-logo'>
            <Link to='/' className='social-logo'>
            <img src={data.message[0].company_logo} alt='logo'/>
            </Link>
          </div>
          <small class='website-rights'>{data1.message[0].company_name} © 2022</small>
         
        </div>
      </section>
    </div>
  );
}

export default Footer;
