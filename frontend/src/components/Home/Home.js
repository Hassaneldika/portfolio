import React, { useState, useEffect } from 'react';
import './Home.css';
import Category from '../Category/category';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';

function Home() {
  const [page, setPage] = useState(6);
  const [num, setNum] = useState(13);
  const [data, setData] = useState(null);
  const [data2, setData2] = useState(null);
  const [data3, setData3] = useState(null);
  const [data4, setData4] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loading2, setLoading2] = useState(true);
  const [loading3, setLoading3] = useState(true);
  const [loading4, setLoading4] = useState(true);
  const [show_more, setShow_more] = useState('show_more');
  const [catName, setCatName] = useState('All');

  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showMore = () => {
    let temp = page;
    setPage(temp+6);
  }

  const hideBtn = () => {
    if(page >= num){
      setShow_more('hide_btn');
    } else {
     setShow_more('show_more');
    }
  }

  useEffect(() => {
    FetchData();
    FetchData1();
    FetchData2();
    hideBtn();
  }, [page]);

  const FetchData = async () => {
    await fetch("http://localhost:3001/")
      .then(response => {
        if (response.ok) {
          return response.json();
        } throw response;
      })
      .then(data => {
        setData(data);
        setLoading(false);
      })
      .catch(error => {
        console.error('Error fetching Data, ' + error);
        setError(error);
      })
  }

  const FetchData1 = async () => {
    await fetch("http://localhost:3001/pages")
      .then(response => {
        if (response.ok) {
          return response.json();
        } throw response;
      })
      .then(data2 => {
        setData2(data2);
        setLoading2(false);
      })
      .catch(error => {
        console.error('Error fetching Data, ' + error);
        setError(error);
      })
  }

  const FetchData2 = async () => {
    await fetch("http://localhost:3001/products")
      .then(response => {
        if (response.ok) {
          return response.json();
        } throw response;
      })
      .then(data3 => {
        setData3(data3);
        setLoading3(false);
      })
      .catch(error => {
        console.error('Error fetching Data, ' + error);
        setError(error);
      })
  }

  if (loading || loading2 || loading3 ) return "Loading data...";
  if (error) return "Error...";

  const findProducts = (name, id) => {
    let Name = name;
    let ID = id;
    let msg = { ID, Name };
    setLoading4(true);

    fetch("http://localhost:3001/readit", {
      method: 'POST',
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(msg)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        } throw response;
      })
      .then(data4 => {
        setData4(data4);
        setLoading4(false);
        setPage(6);
        setNum(data4.length);
      })
      .catch(error => {
        console.error('Error fetching Data, ' + error);
        setError(error);
      })
  }

  const showAll = () => {
    setData4(null);
    setPage(6);
  }

  const getLength = (x) => {
    setNum(x.length);
  }

  return (
    <>
      <Navbar />
      <div className='hero-container'>
        <header>
          <img className='header_img' src={data2.message[0].home_image} alt="header-img" />
          <h1 className='header_title'>{data2.message[0].home_title}</h1>
          <p className='header_desc'>{data2.message[0].home_description}</p>
        </header>
        <main>
          <div className='category_selector' onClick={handleClick}>
            <p>{catName}</p>
          </div>
          <ul className={click ? 'categories_name active' : 'categories_name'}>
            <li onClick={() => { closeMobileMenu(); showAll(); getLength(data3.message); setCatName('All');}}>
              <p>All</p>
            </li>
            {data.message.map((info) =>
              <li onClick={() => { closeMobileMenu(); findProducts(info.name, info._id); setCatName(info.name); hideBtn();}}>
                <p>{info.name}</p>
              </li>
            )}
          </ul>
          <h1 className='title'>List of Products:</h1>
          <div className='products'>
            {!data4 ?
              data3.message.slice(0, page).map((info) =>
                <Category name={info.name} img={info.image} category={info.category.name} price={info.price} discount={info.discount} id={info._id} />
              ) :
              data4.slice(0, page).map((info) =>
                <Category name={info.name} img={info.image} category={info.category.name} price={info.price} discount={info.discount} id={info._id} />
              )
            }
          </div>
        </main>
        <button className={show_more} onClick={()=>{showMore(); hideBtn();}}>Show more</button>
      </div>
      <Footer />
    </>
  );
}

export default Home;
