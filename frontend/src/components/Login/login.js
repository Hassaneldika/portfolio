import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import './login.css';

export default function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [auth, setAuth] = useState('auth');

    const handleSubmit = async (e) => {
        e.preventDefault();
        let msg = {username, password};
        setLoading(true);

        await fetch("http://localhost:3001/admin", {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(msg)
        })
          .then(response => {
            if (response.ok) {
              return response.json();
            } throw response;
          })
          .then(data => {
            setData(data);
            setLoading(false);
            if(data.user) {
              setAuth('auth');
              localStorage.setItem('token', data.user);
              window.location.href = '/login-info';
            } else {
              setAuth('not_auth');
            }
          })
          .catch(error => {
            console.error('Error fetching Data, ' + error);
            setError(error);
          })
    }
        if(loading) return "Loading data...";
        if(error) return "Error...";

    return (
        <div className='login_container'>
            <br />
            <div className="login">
                <h2 className="login_title"> Login </h2>
                <form className='login_form' onSubmit={handleSubmit}>

                    <input type="text" className="text" id='username' name="username" required value={username} onChange={(e) => setUsername(e.target.value)}/>
                    <label for='username' className='username'>username</label>
                    <br />
                    <br />
                    <input type="password" className="text" id='password' name="password" required value={password} onChange={(e) => setPassword(e.target.value)}/>
                    <label for="password" className='password'>password</label>
                    <br />
                    <p className={auth}>Please enter a valid username and password</p>
                    <button className="signin">
                        Log In
                    </button>
                    <br />
                    <br />
                    <Link to={'/'} className='return_home'>Return to Home Page</Link>
                </form>
            </div>
        </div>
    )
}