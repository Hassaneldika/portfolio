import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import './Navbar.css'

function Navbar() {
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click)
    const closeMobileMenu = () => setClick(false)
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);

  useEffect(() => {
    FetchData7();
}, [])

const FetchData7 = async () => {
  await fetch("http://localhost:3001/info")
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
}

if (error) return "Error...";
if (loading) return "Loading data...";

  return (
    <>
      <nav className='navbar'>
        <div className='navbar-container'>
            <Link to="/" className='navbar-logo' onClick={closeMobileMenu}>
                <img src={data.message[0].company_logo} alt='logo'/>
            </Link>
            <div className='menu-icon' onClick={handleClick}>
                <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
            </div>
            <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                <li className='nav-item'>
                    <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                        Home
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to='/about' className='nav-links' onClick={closeMobileMenu}>
                        About
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to='/contact' className='nav-links' onClick={closeMobileMenu}>
                        Contact
                    </Link>
                </li>
            </ul>
        </div>
      </nav>
    </>
  );
}

export default Navbar
