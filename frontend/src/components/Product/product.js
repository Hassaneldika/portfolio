import React, {useEffect, useState} from 'react';
import {Link, useParams} from 'react-router-dom';
import './product.css';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer';

export default function Product() {
  const {id} = useParams()
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    FetchData5(id);
  }, [])

  const FetchData5 = async (id) => {
    await fetch(`http://localhost:3001/product/${id}`)
    .then(response => {
      if (response.ok){
        return response.json();
      } throw response;
    })
    .then(data => {
      setData(data);
      setLoading(false);
    })
    .catch(error => {
      console.error('Error fetching Data, ' + error);
      setError(error);
    })
  }

  if (loading) return "Loading data...";
  if (error) return "Error...";

    return(
        <div className='product_container'>
            <Navbar />
            <div className='product-wrapper'>
            <div className='product_left'>
                <h1 className='product-name'>{data.message[0].name}</h1>
                <h5 className='product-desc'>{data.message[0].description}</h5>
                <h3 className='original_price'><s>{data.message[0].discount !== 0 ? '$'+data.message[0].price : null}</s></h3>
                <h2 className='final_price'>{data.message[0].discount !== 0 ?'$'+(data.message[0].price*(100-data.message[0].discount)/100) : `$${data.message[0].price}`}</h2>
                <h5 className='final_discount'>{data.message[0].discount !== 0 ? data.message[0].discount + '%': null}</h5>
                <Link to={'/'} className='return_to_home'>Return to Home Page</Link>
            </div>
            <div className='product_right'>
                <img src={data.message[0].image} alt='Product' className='product_img'/>
            </div>
            </div>
            <Footer />
        </div>
    )
}