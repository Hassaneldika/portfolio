import React from "react";

function message({ name, message, email, time }) {
    
  return (
    <div className="box">
      <div className="emails_info_header">
        <h4>{name}</h4>
        <div className="email">{email}</div>
      </div>

      <p>{message}</p>

      <time>{time}</time>
    </div>
  );
}

export default message;
